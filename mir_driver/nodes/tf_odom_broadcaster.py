#!/usr/bin/env python3 
import rospy

# Because of transformations
import tf_conversions
import tf2_ros
from geometry_msgs.msg import Pose
from geometry_msgs.msg import TransformStamped

p = Pose()

def handle_turtle_pose(msg, robotframe):
    br = tf2_ros.TransformBroadcaster()
    t = TransformStamped()

    t.header.stamp = rospy.Time.now()
    t.header.frame_id = "odom"
    t.child_frame_id = robotframe
    t.transform.translation.x = msg.position.x
    t.transform.translation.y = msg.position.y
    t.transform.translation.z = msg.position.z
    t.transform.rotation.x = msg.orientation.x
    t.transform.rotation.y = msg.orientation.y
    t.transform.rotation.z = msg.orientation.z
    t.transform.rotation.w = msg.orientation.w

    br.sendTransform(t)

if __name__ == '__main__':
    rospy.init_node('tf_odom_broadcaster')
    print("starting odom -> base_footprint tf publisher")
    rospy.Subscriber("/robot_pose",Pose,handle_turtle_pose,"base_footprint") #/robot_pose
    rospy.spin()
